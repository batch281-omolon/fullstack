import { Container, Navbar, Nav } from "react-bootstrap";
import { NavLink, Link } from "react-router-dom";
import { useContext } from "react";
import UserContext from "../UserContext";

export default function AppNavBar() {
  const { user } = useContext(UserContext);

  return (
    <Navbar bg="dark" data-bs-theme="dark" expand="lg">
      <Container fluid>
        <Navbar.Brand as={Link} to="/">
          Sugbo Skate Warehouse
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="ms-auto">
            <Nav.Link as={NavLink} to="/">
              Home
            </Nav.Link>
            <Nav.Link as={NavLink} to="/products">Shop</Nav.Link>
            {user.isAdmin ? (
              <Nav.Link as={NavLink} to="/admin">
                Admin Panel
              </Nav.Link>
            ) : null}
            {user.id === null || user.id === undefined ? (
              <>
                <Nav.Link as={NavLink} to="/register">
                  Register
                </Nav.Link>
                <Nav.Link as={NavLink} to="/login">
                  Login
                </Nav.Link>
              </>
            ) : (
              <Nav.Link as={NavLink} to="/logout">
                Logout
              </Nav.Link>
            )}
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}
