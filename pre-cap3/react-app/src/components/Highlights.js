import Carousel from 'react-bootstrap/Carousel';

export default function Highlights() {
  return (
    <Carousel>
      <Carousel.Item>
        <img
          className="d-block w-100"
          src="https://coresites-cdn-adm.imgix.net/sidewalk/wp-content/uploads/2017/06/skateboard-brands-an-A-Z-featured-image3.jpg?fit=crop"
          alt="First slide"
        />
        <Carousel.Caption>
          <h3></h3>
          <p></p>
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item>
        <img
          className="d-block w-100"
          src="https://sageisland.com/wp-content/uploads/2018/04/warehouse-skateboards-ecommerce-digital-banner-ads.jpg"
          alt="Second slide"
        />

        <Carousel.Caption>
          <h3></h3>
          <p></p>
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item>
        <img
          className="d-block w-100"
          src="https://www.tennessean.com/gcdn/presto/2022/10/04/PNAS/39752aa3-c3ef-4b96-b2d8-aba572f51f64-skateshop003.JPG"
          alt="Third slide"
        />

        <Carousel.Caption>
          <h3></h3>
          <p></p>
        </Carousel.Caption>
      </Carousel.Item>
    </Carousel>
  );
}

