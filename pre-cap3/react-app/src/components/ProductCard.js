import { Card, Button } from 'react-bootstrap';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

export default function ProductCard({ productProp }) {
  const { _id, productName, productDescription, productPrice, image } = productProp;

  return (
    <Card className="d-flex" style={{width: '18rem'}} >
      <Card.Img variant="top" src={image} alt={productName} />
      <Card.Body>
        <Card.Title>{productName}</Card.Title>
        <Card.Subtitle>Description:</Card.Subtitle>
        <Card.Text>{productDescription}</Card.Text>
        <Card.Subtitle>Price:</Card.Subtitle>
        <Card.Text>{`₱ ${productPrice}`}</Card.Text>
        <Button as={Link} to={`/products/${_id}`} variant="dark">
          Details
        </Button>
      </Card.Body>
    </Card>
  );
}

ProductCard.propTypes = {
  productProp: PropTypes.shape({
    _id: PropTypes.string.isRequired,
    productName: PropTypes.string.isRequired,
    productDescription: PropTypes.string.isRequired,
    productPrice: PropTypes.number.isRequired,
  }).isRequired,
};
