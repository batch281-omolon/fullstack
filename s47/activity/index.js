const txtFirstName = document.querySelector('#txt-first-name');
const txtLastName = document.querySelector('#txt-last-name');
const spanFullName = document.querySelector('#span-full-name');


/*txtFirstName.addEventListener('keyup', (event) => {
	spanFullName.innerHTML = txtFirstName.value;
})*/

txtFirstName.addEventListener('keyup', (event) => {
	console.log(event.target);
	console.log(event.target.value);
})

txtLastName.addEventListener('keyup', (event) => {
	console.log(event.target);
	console.log(event.target.value);
})

const updateFullName = () => {
	const fullName = txtFirstName.value + ' ' + txtLastName.value;
	spanFullName.innerHTML = fullName;
};

txtFirstName.addEventListener('keyup', updateFullName);
txtLastName.addEventListener('keyup', updateFullName);