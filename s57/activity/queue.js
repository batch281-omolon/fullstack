let collection = [];

// Write the queue functions below.

function print() {
    // It will show the array
    return collection;

}

function enqueue(element) {
    //In this function you are going to make an algo that will add an element to the array (last element)
    let newCollection = [];
    let index = 0;
    while (index < size()){
        newCollection[index] = collection[index];
        index++
    }
    newCollection[index] = element;
    collection = newCollection;
    return collection;
}

function dequeue() {
    // In here you are going to remove the first element in the array
    if (isEmpty()) { 
        return undefined; 
    }
    let firstElement = collection[0]; 
    let newCollection = []; 
    let index = 1; 
    while (index < size()) { 
        newCollection[index - 1] = collection[index]; 
        index++; 
    }
    collection = newCollection; 
    return collection; 
}

function front() {
    // you will get the first element
    return collection[0];
}

// starting from here, we cannot use .length method

function size() {
     // Number of elements 
     let count = 0;
     for (let item of collection) {
        count++;
     }
     return count;  
}

function isEmpty() {
    //it will check whether the function is empty or not
    return size() === 0;
}

module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};